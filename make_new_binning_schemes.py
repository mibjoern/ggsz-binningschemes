# This script updates and renames the binning scheme files to make them a bit more logical

import ROOT
import os

def new_file(old_file, new_file):
    print ("Making new file: {}  <-   {}".format(new_file, old_file))
    os.system("cp {} {}".format(old_file, new_file))


def new_file_and_reorder(old_file, new_file):
    print ("Making new file and reversing bins: {}  <-   {}".format(new_file, old_file))

    f = ROOT.TFile(old_file, "r")
    h = f.Get("dkpp_bin_h")
    f_out = ROOT.TFile(new_file, "recreate")
    h2 = ROOT.TH2F(h)

    for i in range(1, h2.GetSize()):
        n = h2.GetBinContent(i)
        if n:
            h2.SetBinContent(i, 9-n)

    f_out.Write("", ROOT.TObject.kOverwrite)
    f.Close()
    f_out.Close()
    return True

if __name__ == "__main__":

    # The KsKK files are alrigt, so just copy them
    new_file("old_files/KsKK_2bins.root",  "./KsKK_2bins.root")
    new_file("old_files/KsKK_3bins.root",  "./KsKK_3bins.root")
    new_file("old_files/KsKK_4bins.root",  "./KsKK_4bins.root")
    new_file("old_files/KsKK_rect.root",  "./KsKK_rect.root")

    # Two of the KsPiPi files are ok and just need sensible names
    new_file("old_files/dkpp_babar.root",  "./KsPiPi_equal.root")
    new_file("old_files/KsPiPi_rect.root",  "./KsPiPi_rect.root")

    # The optimal and modified optimal needs reordering
    new_file_and_reorder("old_files/dkpp_blur.root", "./KsPiPi_optimal.root") # actually CLEO 'optimal'
    new_file_and_reorder("old_files/Optimdkpp_blur.root", "./KsPiPi_mod_optimal.root") # actually CLEO 'modified optimal'