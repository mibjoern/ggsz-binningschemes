# Binning scheme files
These files define the binning schemes over the $D\to K_S hh$ Dalitz plane.

The files exist in two different verisions:
* In the 'old_files' director, one finds the files used up until the 2015+16 GGSZ analysis. They suffer from: the bin order being reversed in the optimal and modified optimal binning schemes for $D\to K_S \pi\pi$, and from unclear file names. But are included here for reference.
* The files in this directory have those issues fixed: the numbering is correct, and the file names are more logical.

For reference, both sets are included and the "translation script" 'new_binning_files.py' is provided here.

In all cases, only the *absolute* bin number is provided, and one has to multiply it with minus one, depending one convention of whether $m(K_Sh^-)<m(K_S h^+)$ corresponds to negative or positive bin numbers. (Opposite conventions are used in the CLEO paper, and in the GGSZ code).

The new (old) binning schemes refer to:
